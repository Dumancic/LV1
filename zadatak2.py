while True:
    try:
        x=float(input("Unesi ocjenu:"))
        break
    except ValueError:
        print("Ocjena nije unešena u odgovarajućem formatu!")
if (x<0 or x>1):
    print("Broj nije u intervalu!")
elif(x>=0.9):
    print ("A")
elif(x>=0.8):
    print ("B")
elif(x>=0.7):
    print ("C")
elif(x>=0.6):
    print ("D")
else:
    print ("F")
        
            